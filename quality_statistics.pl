#!/usr/bin/env perl

#** @file quality_statistics.pl
# @brief    Parse QUAL files and generate R code for boxplots of quality
#           statistics
# @author   Yozen Hernandez
# @date     2012-08-23
#
# Longer description here...
#*

use 5.016; # use strict on by default now
use warnings;
use Bio::SeqIO;
use List::Util qw(sum);
use Data::Dumper;
use Getopt::Long qw(:config gnu_getopt);
use Pod::Usage;

################################################################################
# Option parsing
################################################################################
my $in_format = "qual";     ## Default to QUAL format
my $variant   = "sanger";
my %opts;
GetOptions(
    \%opts, "help|h", "man|m",
    "input|i=s" => \$in_format,
    "var|v=s"   => \$variant,
) or pod2usage(2);

pod2usage(1) if $opts{"help"};
pod2usage( -exitstatus => 0, -verbose => 2 ) if $opts{"man"};

################################################################################
# Main
################################################################################

die "Usage: $0 <QUAL file 1> <QUAL file 2> ..."
  unless @ARGV > 0;

my @pos_qual;    #**< Store qualities for each position
my $read_count = 0;    #**< Store number of reads

# Handle FASTQ variants if FASTQ input
$in_format = $in_format . '-' . $variant
  if ( $in_format eq "fastq" );

# Go through each QUAL file provided on the command line
for my $qualfile (@ARGV) {
    open my $gzqual, "gunzip -c $qualfile |"
      or die "Error opening stream: $!";

    my $qualio = Bio::SeqIO->new( -fh => $gzqual, -format => $in_format );

    while ( my $q = $qualio->next_seq ) {
        my $q = $qualio->next_seq;

        next
          unless defined($q);

        my @quals = @{ $q->qual };

        #print "@quals\n";

        for my $pos ( 0 .. @quals - 1 ) {
            if ( $quals[$pos] < 0 ) {
                warn "Warning: Quality score encountered that is < 0 on read "
                  . $q->display_id
                  . " position "
                  . $pos . "\n";
                next;
            }

            $pos_qual[$pos]->[ $quals[$pos] ] += 1;
        }

        $read_count++;
    }
}

no warnings;
for my $pos ( 0 .. @pos_qual - 1 ) {
    my $sum = 0;

    while ( my ( $qual, $freq ) = each @{ $pos_qual[$pos] } ) {
        $sum += $qual * $freq;
        say STDERR "$pos\t$qual\t$freq";
    }

    my $count = sum( @{ $pos_qual[$pos] } );
    my $avg   = $sum / $count;
    say "$pos\t$avg";
}

# say Dumper( \@pos_qual );

################# POD Documentation ##################

__END__

=head1 NAME

quality_statistics.pl - Quality statistics aggregation for FASTQ and
QUAL format sequence data

=head1 SYNOPSIS

B<quality_statistics.pl> [options] <QUAL file 1> <QUAL file 2> ...

=head1 DESCRIPTION

B<quality_statistics.pl> takes a list of quality score files and
produces a boxplot representing the quality statistics of the
sequence, by position. Currently it only reads in QUAL and FASTQ
format files

=head1 OPTIONS

=over 4

=item B<--help, -h>

Print a brief help message and exits.

=item B<--man, -m>

Prints the manual page and exits.

=item B<--input, -i> 'format'

Input file format. By default, this is 'qual'.

=item B<--variant, -v> 'format'

FASTQ format variant. By default, this is 'sanger'. (Ignored if not 
using FASTQ format).

=back

=head1 EXAMPLES

Section under construction...

=head1 REQUIRES

Perl 5.016, BioPerl

=head1 SEE ALSO

  perl(1)

=head1 AUTHORS

 Yözen Hernández yzhernand at gmail dot com

=cut

##################### End ##########################
