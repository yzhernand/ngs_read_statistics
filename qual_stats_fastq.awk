BEGIN {
  headertype="";
  max_rlen=0;
  qual_start=0;
  qual_end=93;
  qual_off=33;

  # Lookup table of ASCII chars and their quality value (PHRED only!)
  for (q=qual_start;q<=qual_end;q++) {
    char=q+qual_off;
    chr2qual[sprintf("%c",char)]=q;
  }
}
{
  if($0 ~ "^@") {
    countread++;
    headertype="@";
  }
  else if($0 ~ "^+") {
    headertype="+";
  }
  else if(headertype=="@") { # This is a nuc sequence
    len=length($0);
    if (len>4) {
      readlength[len]++;
      if (len>max_rlen){
        max_rlen=len; # Save longest read length
      }
    }
    headertype="";
  }
  else if(headertype=="+") {
    len=length($0);
    split($0, quals, "");

    for (i=1;i<=length(quals);i++) {
      score=chr2qual[quals[i]];
      #printf("Storing i = %d and q = %d given char = %s\n",i,chr2qual[quals[i]], quals[i]);
      pos[i,score]++;
    }
    headertype="";
  }
}
END {
  for (i in readlength){
    countstored+=readlength[i];
    lensum+=readlength[i]*i;
    print i, readlength[i];
  }
  for (i=1;i<=max_rlen;i++){
    pos_sum=0;
    pos_count=0;
    for (q=qual_start;q<=qual_end;q++){
      #printf("Using i = %d and q = %d\n",i,q);
      if ((i,q) in pos){
        print i"\t"q"\t"pos[i,q];
        pos_sum+=pos[i,q]*q;
        pos_count++;
      }
      else {
        print i"\t"q"\t"0;
      }
    }
    printf("%d\t%f\n",i,pos_sum/pos_count) > "/dev/stderr";
  }
  print "read_count\t"countread > "/dev/stderr";
  #print "reads_stored\t"countstored > "/dev/stderr";
  print "read_avglen\t"lensum/countstored > "/dev/stderr";
}
