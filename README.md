# NGS Read statistics

This is meant so be a collection of scripts which do some simple statistical
analyses on next-generation sequencing (NGS) reads.

## Types of analyses


### Quality statistics

A perl script parses through a set of gzipped read files. It can handle FASTA
and FASTQ format files

### Average read length and coverage

